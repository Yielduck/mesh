#include <mesh/complete_mesh.inl>
#include <mesh/mesh_traits.inl>
#include <mesh/mesh_view.h>
#include <cstdint>
#include <iostream>
#include <cmath>

struct CylinderMesh
{
    using idx_t = std::uint32_t;
    struct point_t
    {
        float x, y, z;
    };

    idx_t dim_r = 0;
    idx_t dim_phi = 0;
    idx_t dim_z = 0;
    point_t point(idx_t const cell_id, idx_t const face_id, idx_t const point_id) const noexcept
    {
        idx_t const iz = cell_id / (dim_r * dim_phi);
        idx_t const iy = (cell_id - iz * dim_z) / dim_r % dim_phi;
        idx_t const ix = (cell_id - iz * dim_z) % dim_r;

        idx_t const f[6][4] =
        {
            {0, 1, 3, 2},
            {4, 5, 7, 6},
            {0, 1, 5, 4},
            {2, 3, 7, 6},
            {0, 2, 6, 4},
            {1, 3, 7, 5},
        };
        idx_t const i = f[face_id][point_id];

        double const x = static_cast<double>(ix + (i % 2 != 0 ? 1 : 0));
        double const y = static_cast<double>(iy + ((i / 2) % 2 != 0 ? 1 : 0));
        double const z = static_cast<double>(iz + (i / 4 != 0 ? 1 : 0));

        double const r = 0.8 * x / dim_r + 0.2;
        double const phi = y == dim_phi ? 0. : (2. * M_PI) * (y / dim_phi);
        return point_t
        {
            static_cast<float>(r * std::cos(phi)),
            static_cast<float>(r * std::sin(phi)),
            static_cast<float>(z / dim_z),
        };
        //return {x, y, z};
    }
    idx_t points(idx_t const, idx_t const) const noexcept {return 4;}
    idx_t faces(idx_t const) const noexcept {return 6;}
    idx_t cells() const noexcept {return dim_r * dim_phi * dim_z;}
};

using ParentMesh = mesh::CompleteMesh<CylinderMesh::point_t, CylinderMesh::idx_t>;
struct Mesh final : public ParentMesh
{
    template<typename OtherMesh>
    Mesh(OtherMesh const * const mesh) noexcept
        : ParentMesh(mesh)
    {
        update();
    }

    template<typename Readable>
    Mesh(Readable const &readable) 
    {
        ParentMesh::mm_deserialize(readable);
        update();
    }
    template<typename Writable>
    void write(Writable const &writable) const
    {
        ParentMesh::mm_serialize(writable);
    }
};
struct vec3d
{
    double x, y, z;
};

int main()
{
    static_assert(mesh::ImplementsSimpleMeshInterface<CylinderMesh>::value);
    CylinderMesh const cylinder = {10, 20, 3};
    Mesh const mesh(&cylinder);

    /*
    auto const readable = +[](std::byte *memory, std::size_t sz) -> void
    {
        if(!std::cin.read(reinterpret_cast<char *>(memory), static_cast<std::streamsize>(sz)))
            throw std::exception();
    };
    Mesh const mesh(readable);
    */
    /*
    auto const writeable = +[](std::byte const *memory, std::size_t sz) -> void
    {
        if(!std::cout.write(reinterpret_cast<char const *>(memory), static_cast<std::streamsize>(sz)))
            throw std::exception();
    };
    mesh.write(writeable);
    */

    std::cout << mesh::MeshTraits<Mesh>::cellVolume(&mesh, 0) << std::endl;

    mesh::MeshView<Mesh const> const view = &mesh;
    std::cout << view.cellVolume(0) << std::endl;
}
