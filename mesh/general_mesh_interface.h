#pragma once
#include "simple_mesh_interface.h"
#include "span.h"
namespace mesh
{
/*
 * although SimpleMeshInterface is easy to implement, it is difficult to be dealed with
 * as we have no way of figuring out which points or faces, defined by local indices, are the same in a global sense
 * global indexation of points and faces gives the way
 * so we extend SimpleMeshInterface to the following
 */
template<typename vec3, typename uint>
struct GeneralMeshInterface : public SimpleMeshInterface<vec3, uint>
{
    using point_t   = typename SimpleMeshInterface<vec3, uint>::point_t;
    using idx_t     = typename SimpleMeshInterface<vec3, uint>::idx_t;

    using face_t    = span<idx_t const>;       // span of pointID
    using cell_t    = span<idx_t const>;       // span of faceID

    // (!) spans, returned by a method (face_t, cell_t and the others)
    // (!) must remain valid until the next call of the same method

    virtual idx_t   points  ()              const noexcept = 0;
    virtual idx_t   faces   ()              const noexcept = 0;
    virtual idx_t   cells   ()              const noexcept override = 0;

    virtual point_t point   (idx_t pointID) const noexcept = 0;
    virtual face_t  face    (idx_t faceID)  const noexcept = 0;
    virtual cell_t  cell    (idx_t cellID)  const noexcept = 0;

    // partial implementation of SimpleMeshInterface
    point_t point(idx_t cellID, idx_t faceID, idx_t pointID) const noexcept override
    {
        return point(face(cell(cellID)[faceID])[pointID]);
    }
    idx_t points(idx_t cellID, idx_t faceID) const noexcept override
    {
        return static_cast<idx_t>(face(cell(cellID)[faceID]).size());
    }
    idx_t faces(idx_t cellID) const noexcept override
    {
        return static_cast<idx_t>(cell(cellID).size());
    }
};

template<typename, typename = std::void_t<>>
struct ImplementsGeneralMeshInterface : std::false_type
{};
template<typename T>
struct ImplementsGeneralMeshInterface
<
    T,
    std::void_t
    <
        typename T::idx_t,
        typename T::point_t,
        decltype(std::declval<T const>().point(0u, 0u, 0u)),
        decltype(std::declval<T const>().points(0u, 0u)),
        decltype(std::declval<T const>().faces(0u)),
        decltype(std::declval<T const>().cells()),

        typename T::face_t,
        typename T::cell_t,
        decltype(std::declval<T const>().point(0u)),
        decltype(std::declval<T const>().face(0u)),
        decltype(std::declval<T const>().cell(0u)),
        decltype(std::declval<T const>().points()),
        decltype(std::declval<T const>().faces())
    >
> : std::conjunction
    <
        ImplementsSimpleMeshInterface<T>,

        std::is_same<typename T::point_t, decltype(std::declval<T const>().point(0u))>,
        std::is_same<typename T::face_t,  decltype(std::declval<T const>().face(0u))>,
        std::is_same<typename T::cell_t,  decltype(std::declval<T const>().cell(0u))>,
        std::is_same<typename T::idx_t,   decltype(std::declval<T const>().points())>,
        std::is_same<typename T::idx_t,   decltype(std::declval<T const>().faces())>
    >
{};

} // namespace mesh
