#pragma once
#include "mesh_view.h"
#include <vector>
namespace mesh
{

template<typename Mesh>
struct FilterMesh final : public MeshView<Mesh const>
{
    using Parent = MeshView<Mesh const>;

    using point_t   = typename Mesh::point_t;
    using idx_t     = typename Mesh::idx_t;

    using face_t    = typename Mesh::face_t;
    using cell_t    = typename Mesh::cell_t;

    using SimpleMeshInterface<point_t, idx_t>::point;
    using SimpleMeshInterface<point_t, idx_t>::points;
    using SimpleMeshInterface<point_t, idx_t>::faces;
    using Parent::point;
    using Parent::points;
    using Parent::face;
    using Parent::faces;

    using Parent::pointFaces;
    using Parent::pointCells;

    using Parent::faceEdges;
    using Parent::faceCells;
    using Parent::faceNorm;
    using Parent::faceCenter;

    using Parent::cellPoints;
    using Parent::cellEdges;
    using Parent::cellCenter;
    using Parent::cellVolume;

    std::vector<idx_t> cellMap;
    cell_t cell(idx_t const cellID) const noexcept override {return Parent::cell(cellMap[cellID]);}
    idx_t cells() const noexcept override {return static_cast<idx_t>(cellMap.size());}

    template<typename Filter>
    void apply(Filter &&filter) noexcept
    {
        idx_t const cellCount = cells();
        cellMap.clear();
        for(idx_t cellID = 0; cellID < cellCount; ++cellID)
            if(filter(cellID))
                cellMap.push_back(cellID);
    }

    FilterMesh(Mesh const * const ptr) noexcept
        : MeshView<Mesh const>(ptr)
    {
        idx_t const cellCount = Parent::cells();
        cellMap.reserve(cellCount);
        for(idx_t cellID = 0; cellID < cellCount; ++cellID)
            cellMap.push_back(cellID);
    }
};

} // namespace mesh
