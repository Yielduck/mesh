#pragma once
#include "mesh_traits.h"
#include "point_traits.inl"
#include <cassert>
#include <algorithm>
namespace mesh
{

namespace detail
{

template<typename Mesh>
auto pointCellsImpl(Mesh const * const mesh, typename Mesh::idx_t const pointID,   signed int) noexcept
    -> decltype(mesh->pointCells(pointID))
{
    return mesh->pointCells(pointID);
}
template<typename Mesh>
auto pointCellsImpl(Mesh const * const mesh, typename Mesh::idx_t const pointID, unsigned int) noexcept
    -> span<typename Mesh::idx_t const>
{
    assert(pointID < mesh->points());
    static thread_local std::vector<typename Mesh::idx_t> pointCells;
    pointCells.clear();
    span<typename Mesh::idx_t const> const pointFaces = mesh->pointFaces(pointID);
    for(typename Mesh::idx_t const faceID : pointFaces)
    {
        typename Mesh::link_t const link = mesh->faceCells(faceID);
        pointCells.push_back(link.first);
        pointCells.push_back(link.second);
    }
    std::sort(pointCells.begin(), pointCells.end());
    auto const pointCellsSize = static_cast<typename span<typename Mesh::idx_t const>::size_type>
    (
        std::unique(pointCells.begin(), pointCells.end()) - pointCells.begin()
    );
    return {pointCells.data(), pointCellsSize};
}
template<typename Mesh>
auto faceNormImpl(Mesh const * const mesh, typename Mesh::idx_t const faceID,   signed int) noexcept
    -> decltype(mesh->faceNorm(faceID))
{
    return mesh->faceNorm(faceID);
}
template<typename Mesh>
auto faceNormImpl(Mesh const * const mesh, typename Mesh::idx_t const faceID, unsigned int) noexcept
    -> typename MeshTraits<Mesh>::area_t
{
    assert(faceID < mesh->faces());
    typename Mesh::face_t const face = mesh->face(faceID);
    assert(face.size() > 2);
    using PT = PointTraits<typename Mesh::point_t>;
    typename Mesh::point_t diff0 = PT::sub(mesh->point(face[1]), mesh->point(face[0]));
    typename Mesh::point_t diff1 = PT::sub(mesh->point(face[2]), mesh->point(face[0]));
    typename MeshTraits<Mesh>::area_t area = PT::cross(diff1, diff0);
    for(typename Mesh::idx_t const pointID : face.subspan(3))
    {
        diff0 = diff1;
        diff1 = PT::sub(mesh->point(pointID), mesh->point(face[0]));
        area = PointTraits<typename MeshTraits<Mesh>::area_t>::add(area, PT::cross(diff1, diff0));
    }
    return PointTraits<typename MeshTraits<Mesh>::area_t>::div(area, 2);
}
template<typename Mesh>
auto faceCenterImpl(Mesh const * const mesh, typename Mesh::idx_t const faceID,   signed int) noexcept
    -> decltype(mesh->faceCenter(faceID))
{
    return mesh->faceCenter(faceID);
}
template<typename Mesh>
auto faceCenterImpl(Mesh const * const mesh, typename Mesh::idx_t const faceID, unsigned int) noexcept
    -> typename MeshTraits<Mesh>::point_t
{
    assert(faceID < mesh->faces());
    typename Mesh::face_t const face = mesh->face(faceID);
    assert(face.size() > 0);
    typename Mesh::point_t center = mesh->point(face[0]);
    for(typename Mesh::idx_t const pointID : face.subspan(1))
        center = PointTraits<typename Mesh::point_t>::add(center, mesh->point(pointID));
    return PointTraits<typename Mesh::point_t>::div(center, face.size());
}
template<typename Mesh>
auto faceEdgesImpl(Mesh const * const mesh, typename Mesh::idx_t const faceID,   signed int) noexcept
    -> decltype(mesh->faceEdges(faceID))
{
    return mesh->faceEdges(faceID);
}
template<typename Mesh>
auto faceEdgesImpl(Mesh const * const mesh, typename Mesh::idx_t const faceID, unsigned int) noexcept
    -> span<typename MeshTraits<Mesh>::edge_t const>
{
    assert(faceID < mesh->faces());
    static thread_local std::vector<typename MeshTraits<Mesh>::edge_t> faceEdges;
    faceEdges.clear();
    typename Mesh::face_t const face = mesh->face(faceID);
    for(decltype(face.size()) i = 0; i + 1 < face.size(); ++i)
        faceEdges.emplace_back(face[i] < face[i + 1] ? face[i] : face[i + 1],
                               face[i] < face[i + 1] ? face[i + 1] : face[i]);
    if(face.size() > 2)
    {
        auto const N = face.size() - 1;
        faceEdges.emplace_back(face[0] < face[N] ? face[0] : face[N],
                               face[0] < face[N] ? face[N] : face[0]);
    }
    return {faceEdges.data(), faceEdges.size()};
}
template<typename Mesh>
auto cellPointsImpl(Mesh const * const mesh, typename Mesh::idx_t const cellID,   signed int) noexcept
    -> decltype(mesh->cellPoints(cellID))
{
    return mesh->cellPoints(cellID);
}
template<typename Mesh>
auto cellPointsImpl(Mesh const * const mesh, typename Mesh::idx_t const cellID, unsigned int) noexcept
    -> span<typename Mesh::idx_t const>
{
    assert(cellID < mesh->cells());
    static thread_local std::vector<typename Mesh::idx_t> cellPoints;
    cellPoints.clear();
    for(typename Mesh::idx_t const faceID : mesh->cell(cellID))
        for(typename Mesh::idx_t const pointID : mesh->face(faceID))
            cellPoints.push_back(pointID);
    std::sort(cellPoints.begin(), cellPoints.end());
    auto const cellPointsSize = static_cast<typename span<typename Mesh::idx_t const>::size_type>
    (
        std::unique(cellPoints.begin(), cellPoints.end()) - cellPoints.begin()
    );
    return {cellPoints.data(), cellPointsSize};
}
template<typename Mesh>
auto cellEdgesImpl(Mesh const * const mesh, typename Mesh::idx_t const cellID,   signed int) noexcept
    -> decltype(mesh->cellEdges(cellID))
{
    return mesh->cellEdges(cellID);
}
template<typename Mesh>
auto cellEdgesImpl(Mesh const * const mesh, typename Mesh::idx_t const cellID, unsigned int) noexcept
    -> span<typename Mesh::edge_t const>
{
    assert(cellID < mesh->cells());
    static thread_local std::vector<typename Mesh::edge_t> cellEdges;
    cellEdges.clear();
    for(typename Mesh::idx_t const faceID : mesh->cell(cellID))
    {
        span<typename Mesh::edge_t const> const faceEdges = faceEdgesImpl(mesh, faceID, 0);
        cellEdges.insert(cellEdges.end(), faceEdges.begin(), faceEdges.end());
    }
    auto const less = [](typename Mesh::edge_t const lhs, typename Mesh::edge_t const rhs) noexcept -> bool
    {
        return lhs.first != rhs.first 
            ? lhs.first < rhs.first
            : lhs.second < rhs.second;
    };
    std::sort(cellEdges.begin(), cellEdges.end(), less);
    auto const cellEdgesSize = static_cast<typename span<typename Mesh::edge_t const>::size_type>
    (
        std::unique(cellEdges.begin(), cellEdges.end()) - cellEdges.begin()
    );
    return {cellEdges.data(), cellEdgesSize};
}
template<typename Mesh>
auto cellCenterImpl(Mesh const * const mesh, typename Mesh::idx_t const cellID,   signed int) noexcept
    -> decltype(mesh->cellCenter(cellID))
{
    return mesh->cellCenter(cellID);
}
template<typename Mesh>
auto cellCenterImpl(Mesh const * const mesh, typename Mesh::idx_t const cellID, unsigned int) noexcept
    -> decltype(mesh->faceCenter(cellID))
{
    assert(cellID < mesh->cells());
    typename Mesh::cell_t const cell = mesh->cell(cellID);
    typename Mesh::point_t center = mesh->faceCenter(cell[0]);
    for(typename Mesh::idx_t const faceID : cell.subspan(1))
        center = PointTraits<typename Mesh::point_t>::add(center, mesh->faceCenter(faceID));
    return PointTraits<typename Mesh::point_t>::div(center, cell.size());
}
template<typename Mesh>
auto cellCenterImpl(Mesh const * const mesh, typename Mesh::idx_t const cellID, double) noexcept
    -> typename Mesh::point_t
{
    assert(cellID < mesh->cells());
    span<typename Mesh::idx_t const> const cellPoints = cellPointsImpl(mesh, cellID, 0);
    assert(cellPoints.size() != 0);
    typename Mesh::point_t center = mesh->point(cellPoints[0]);
    for(typename Mesh::idx_t const pointID : cellPoints.subspan(1))
        center = PointTraits<typename Mesh::point_t>::add(center, mesh->point(pointID));
    return PointTraits<typename Mesh::point_t>::div(center, cellPoints.size());
}
template<typename Mesh>
auto cellVolumeImpl(Mesh const * const mesh, typename Mesh::idx_t const cellID,   signed int) noexcept
    -> decltype(mesh->cellVolume(cellID))
{
    return mesh->cellVolume(cellID);
}
template<typename Mesh>
auto cellVolumeImpl(Mesh const * const mesh, typename Mesh::idx_t const cellID, unsigned int) noexcept
    -> typename MeshTraits<Mesh>::volume_t
{
    assert(cellID < mesh->cells());
    span<typename Mesh::idx_t const> const cell = mesh->cell(cellID);
    assert(cell.size() > 0);
    typename Mesh::point_t const cellCenter = cellCenterImpl(mesh, cellID, 0);
    using PT = PointTraits<typename Mesh::point_t>;
    auto const abs = +[](typename PT::volume_t const v) noexcept
    {
        typename PT::volume_t const zero = 0;
        return v < zero ? -v : v;
    };
    typename PT::volume_t volume = abs(PT::dot(PT::sub(faceCenterImpl(mesh, cell[0], 0), cellCenter), faceNormImpl(mesh, cell[0], 0)));
    for(typename Mesh::idx_t const faceID : cell.subspan(1))
        volume = volume + abs(PT::dot(PT::sub(faceCenterImpl(mesh, faceID, 0), cellCenter), faceNormImpl(mesh, faceID, 0)));
    return volume / 3;
}

} // namespace detail

template<typename Mesh>
span<typename Mesh::idx_t const> MeshTraits<Mesh>::pointFaces(Mesh const * const mesh, idx_t const pointID) noexcept
{
    return mesh->pointFaces(pointID);
}
template<typename Mesh>
span<typename MeshTraits<Mesh>::idx_t const> MeshTraits<Mesh>::pointCells(Mesh const * const mesh, idx_t const pointID) noexcept
{
    return detail::pointCellsImpl(mesh, pointID, 0);
}
template<typename Mesh>
typename MeshTraits<Mesh>::link_t MeshTraits<Mesh>::faceCells(Mesh const * const mesh, idx_t const faceID) noexcept
{
    return mesh->faceCells(faceID);
}
template<typename Mesh>
typename MeshTraits<Mesh>::area_t MeshTraits<Mesh>::faceNorm(Mesh const * const mesh, idx_t const faceID) noexcept
{
    return detail::faceNormImpl(mesh, faceID, 0);
}
template<typename Mesh>
typename MeshTraits<Mesh>::point_t MeshTraits<Mesh>::faceCenter(Mesh const * const mesh, idx_t const faceID) noexcept
{
    return detail::faceCenterImpl(mesh, faceID, 0);
}
template<typename Mesh>
span<typename MeshTraits<Mesh>::edge_t const> MeshTraits<Mesh>::faceEdges(Mesh const * const mesh, idx_t const faceID) noexcept
{
    return detail::faceEdgesImpl(mesh, faceID, 0);
}
template<typename Mesh>
span<typename MeshTraits<Mesh>::idx_t const> MeshTraits<Mesh>::cellPoints(Mesh const * const mesh, idx_t const cellID) noexcept
{
    return detail::cellPointsImpl(mesh, cellID, 0);
}
template<typename Mesh>
span<typename MeshTraits<Mesh>::edge_t const> MeshTraits<Mesh>::cellEdges(Mesh const * const mesh, idx_t const cellID) noexcept
{
    return detail::cellEdgesImpl(mesh, cellID, 0);
}
template<typename Mesh>
typename Mesh::point_t MeshTraits<Mesh>::cellCenter(Mesh const * const mesh, idx_t const cellID) noexcept
{
    return detail::cellCenterImpl(mesh, cellID, 0);
}
template<typename Mesh>
typename MeshTraits<Mesh>::volume_t MeshTraits<Mesh>::cellVolume(Mesh const * const mesh, idx_t const cellID) noexcept
{
    return detail::cellVolumeImpl(mesh, cellID, 0);
}

} // namespace mesh
