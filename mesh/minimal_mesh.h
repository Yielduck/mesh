#pragma once
#include "general_mesh_interface.h"
#include <vector>
namespace mesh
{

template<typename vec3, typename uint>
class MinimalMesh : public GeneralMeshInterface<vec3, uint>
{
public:
    using point_t   = typename GeneralMeshInterface<vec3, uint>::point_t;
    using idx_t     = typename GeneralMeshInterface<vec3, uint>::idx_t;

    using face_t    = typename GeneralMeshInterface<vec3, uint>::face_t;
    using cell_t    = typename GeneralMeshInterface<vec3, uint>::cell_t;

    using SimpleMeshInterface<vec3, uint>::point;
    using SimpleMeshInterface<vec3, uint>::points;
    using SimpleMeshInterface<vec3, uint>::faces;

    MinimalMesh() noexcept;
    template<typename Mesh>
    // Mesh must at least implement SimpleMeshInterface
    // constructs mush faster if Mesh implements GeneralMeshInterface
    MinimalMesh(Mesh const *) noexcept;

    idx_t   points  ()              const noexcept override;
    idx_t   faces   ()              const noexcept override;
    idx_t   cells   ()              const noexcept override;

    point_t point   (idx_t pointID) const noexcept override;
    face_t  face    (idx_t faceID)  const noexcept override;
    cell_t  cell    (idx_t cellID)  const noexcept override;

protected:
    std::vector<point_t> mPoint;    // mPoint       : point_id      -> point_t

    std::vector<idx_t> mFacePoint;  // mFacePoint   : pointIDiter   -> pointID
    std::vector<idx_t> mFace;       // mFace        : faceID        -> pointIDbegin
                                    // mFace        : faceID + 1    -> pointIDend
    std::vector<idx_t> mCellFace;   // mCellFace    : faceIDiter    -> faceID
    std::vector<idx_t> mCell;       // mCell        : cellID        -> faceIDbegin
                                    // mCell        : cellID + 1    -> faceIDend

    template<typename Writable>
    // Writable must implement operator()(std::byte const *, std::size_t) -> void
    void mm_serialize(Writable &&) const;

    template<typename Readable>
    // Readable must implement operator()(std::byte *, std::size_t) -> void
    void mm_deserialize(Readable &&);
};

} // namespace mesh
