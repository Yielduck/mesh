#pragma once
#include <type_traits>
namespace mesh
{
/*
 * mesh = array of cells
 * cell = array of faces
 * face = array of points
 * every array induces internal indexing from 0 to array.size() - 1
 * let this indexing be called `local'
 * based on this, the following interface may be proposed
 */
template<typename vec3, typename uint>
struct SimpleMeshInterface
{
    static_assert(std::is_same_v<vec3, typename std::decay_t<vec3>>);
    static_assert(std::is_unsigned_v<uint>);

    using point_t = vec3;
    using   idx_t = uint;

    virtual point_t point   (idx_t cellID, idx_t faceID, idx_t pointID) const noexcept = 0;
    virtual idx_t   points  (idx_t cellID, idx_t faceID)                const noexcept = 0;
    virtual idx_t   faces   (idx_t cellID)                              const noexcept = 0;
    virtual idx_t   cells   ()                                          const noexcept = 0;

    virtual ~SimpleMeshInterface(){}
};

// when c++20 comes, this helper should be replaced with the concept
template<typename, typename = std::void_t<>>
struct ImplementsSimpleMeshInterface
    : std::false_type
{};
template<typename T>
struct ImplementsSimpleMeshInterface
<
    T,
    std::void_t
    <
        typename T::idx_t,
        typename T::point_t,
        decltype(std::declval<T const>().point(0u, 0u, 0u)),
        decltype(std::declval<T const>().points(0u, 0u)),
        decltype(std::declval<T const>().faces(0u)),
        decltype(std::declval<T const>().cells())
    >
> : std::conjunction
    <
        std::is_same<typename T::point_t, typename std::decay_t<typename T::point_t>>,
        std::is_unsigned<typename T::idx_t>,

        std::is_same<typename T::point_t, decltype(std::declval<T const>().point(0u, 0u, 0u))>,
        std::is_same<typename T::idx_t,   decltype(std::declval<T const>().points(0u, 0u))>,
        std::is_same<typename T::idx_t,   decltype(std::declval<T const>().faces(0u))>,
        std::is_same<typename T::idx_t,   decltype(std::declval<T const>().cells())>
    >
{};

} // namespace mesh
