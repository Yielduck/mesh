#pragma once
#include "minimal_mesh.h"
#include "mesh_traits.h"
namespace mesh
{

template<typename vec3, typename uint>
class CompleteMesh : public MinimalMesh<vec3, uint>
{
    using ParentTraits = MeshTraits<MinimalMesh<vec3, uint>>;
public:
    using point_t   = typename ParentTraits::point_t;
    using idx_t     = typename ParentTraits::idx_t;

    using face_t    = typename ParentTraits::face_t;
    using cell_t    = typename ParentTraits::cell_t;

    using volume_t  = typename ParentTraits::volume_t;
    using area_t    = typename ParentTraits::area_t;

    using edge_t    = typename ParentTraits::edge_t;
    using link_t    = typename ParentTraits::link_t;

    using SimpleMeshInterface<vec3, uint>::point;
    using SimpleMeshInterface<vec3, uint>::points;
    using SimpleMeshInterface<vec3, uint>::faces;
    using MinimalMesh<vec3, uint>::point;
    using MinimalMesh<vec3, uint>::face;
    using MinimalMesh<vec3, uint>::cell;
    using MinimalMesh<vec3, uint>::points;
    using MinimalMesh<vec3, uint>::faces;
    using MinimalMesh<vec3, uint>::cells;

    CompleteMesh() = default;
    template<typename Mesh>
    CompleteMesh(Mesh const * const mesh) noexcept
        : MinimalMesh<vec3, uint>(mesh)
    {}

    span<idx_t const>   pointFaces(idx_t pointID)  const noexcept;

    link_t              faceCells (idx_t faceID)   const noexcept;
    area_t              faceNorm  (idx_t faceID)   const noexcept;
    point_t             faceCenter(idx_t faceID)   const noexcept;

    point_t             cellCenter(idx_t cellID)   const noexcept;
    volume_t            cellVolume(idx_t cellID)   const noexcept;

protected:
    std::vector<idx_t> mPointFacesID;
    std::vector<idx_t> mPointFaces;
    std::vector<link_t> mFaceCells;
    std::vector<area_t> mFaceNorm;
    std::vector<point_t> mFaceCenter;
    std::vector<point_t> mCellCenter;
    std::vector<volume_t> mCellVolume;

    virtual void update() noexcept; // update vectors above with MinimalMesh data
    virtual void clear() noexcept;  // release these vectors
};

} // namespace mesh
