#pragma once
#include "mesh_traits.inl"
namespace mesh
{

template<typename Mesh>
struct MeshView : public GeneralMeshInterface<typename Mesh::point_t, typename Mesh::idx_t>
{
    static_assert(ImplementsGeneralMeshInterface<Mesh>::value);

    Mesh *mesh;
    MeshView(Mesh * const ptr) noexcept
        : mesh(ptr)
    {}

    using point_t   = typename Mesh::point_t;
    using idx_t     = typename Mesh::idx_t;

    using face_t    = typename Mesh::face_t;
    using cell_t    = typename Mesh::cell_t;

    using SimpleMeshInterface<point_t, idx_t>::point;
    using SimpleMeshInterface<point_t, idx_t>::points;
    using SimpleMeshInterface<point_t, idx_t>::faces;

    idx_t   points  ()                      const noexcept override {return mesh->points();}
    idx_t   faces   ()                      const noexcept override {return mesh->faces();}
    idx_t   cells   ()                      const noexcept override {return mesh->cells();}

    point_t point   (idx_t const pointID)   const noexcept override {return mesh->point(pointID);}
    face_t  face    (idx_t const faceID)    const noexcept override {return mesh->face(faceID);}
    cell_t  cell    (idx_t const cellID)    const noexcept override {return mesh->cell(cellID);}

    using Traits    = MeshTraits<Mesh>;

    using area_t    = typename Traits::area_t;
    using volume_t  = typename Traits::volume_t;

    using edge_t    = typename Traits::edge_t;
    using link_t    = typename Traits::link_t;

    span<idx_t const>   pointFaces(idx_t const pointID) const noexcept {return Traits::pointFaces(mesh, pointID);}
    span<idx_t const>   pointCells(idx_t const pointID) const noexcept {return Traits::pointCells(mesh, pointID);}

    span<edge_t const>  faceEdges (idx_t const faceID)  const noexcept {return Traits::faceEdges (mesh, faceID);}
    link_t              faceCells (idx_t const faceID)  const noexcept {return Traits::faceCells (mesh, faceID);}
    area_t              faceNorm  (idx_t const faceID)  const noexcept {return Traits::faceNorm  (mesh, faceID);}
    point_t             faceCenter(idx_t const faceID)  const noexcept {return Traits::faceCenter(mesh, faceID);}

    span<idx_t const>   cellPoints(idx_t const cellID)  const noexcept {return Traits::cellPoints(mesh, cellID);}
    span<edge_t const>  cellEdges (idx_t const cellID)  const noexcept {return Traits::cellEdges (mesh, cellID);}
    point_t             cellCenter(idx_t const cellID)  const noexcept {return Traits::cellCenter(mesh, cellID);}
    volume_t            cellVolume(idx_t const cellID)  const noexcept {return Traits::cellVolume(mesh, cellID);}
};

} // namespace mesh
