#pragma once
#include "minimal_mesh.h"
#include "writable.h"
#include "readable.h"
#include "point_traits.inl"
#include <cstdint>
#include <unordered_map>
namespace mesh
{

template<typename vec3, typename uint>
MinimalMesh<vec3, uint>::MinimalMesh() noexcept
{
    mFace.push_back(0);
    mCell.push_back(0);
}
template<typename vec3, typename uint>
template<typename Mesh>
MinimalMesh<vec3, uint>::MinimalMesh(Mesh const * const mesh) noexcept
{
    static_assert(ImplementsSimpleMeshInterface<Mesh>::value);
    if constexpr(ImplementsGeneralMeshInterface<Mesh>::value)
    {
        typename Mesh::idx_t const pointCount = mesh->points();
        typename Mesh::idx_t const  faceCount = mesh->faces();
        typename Mesh::idx_t const  cellCount = mesh->cells();

        mPoint.reserve(pointCount);
        for(typename Mesh::idx_t pointID = 0; pointID < pointCount; ++pointID)
            mPoint.push_back(PointTraits<typename Mesh::point_t>::template cast<point_t>(mesh->point(pointID)));

        std::size_t facePointCount = 0;
        for(typename Mesh::idx_t faceID = 0; faceID < faceCount; ++faceID)
            facePointCount += mesh->face(faceID).size();
        mFacePoint.reserve(facePointCount);
        mFace.reserve(faceCount + 1);
        mFace.push_back(0);
        for(typename Mesh::idx_t faceID = 0; faceID < faceCount; ++faceID)
        {
            for(typename Mesh::idx_t const pointID : mesh->face(faceID))
                mFacePoint.push_back(static_cast<idx_t>(pointID));
            mFace.push_back(static_cast<idx_t>(mFacePoint.size()));
        }

        std::size_t cellFaceCount = 0;
        for(typename Mesh::idx_t cellID = 0; cellID < cellCount; ++cellID)
            cellFaceCount += mesh->cell(cellID).size();
        mCellFace.reserve(cellFaceCount);
        mCell.reserve(cellCount + 1);
        mCell.push_back(0);
        for(typename Mesh::idx_t cellID = 0; cellID < cellCount; ++cellID)
        {
            for(typename Mesh::idx_t const faceID : mesh->cell(cellID))
                mCellFace.push_back(static_cast<idx_t>(faceID));
            mCell.push_back(static_cast<idx_t>(mCellFace.size()));
        }
    }
    else
    {
        struct idx3
        {
            idx_t data[3];
            idx_t  operator[](unsigned int const i) const noexcept {return data[i];}
            idx_t &operator[](unsigned int const i)       noexcept {return data[i];}
        };
        struct idx3_hash
        {
            std::size_t operator()(idx3 const &i) const noexcept
            {
                return std::hash<idx_t>{}(i[0])
                     ^ std::hash<idx_t>{}(i[1])
                     ^ std::hash<idx_t>{}(i[2]);
            }
        };
        struct idx3_equal
        {
            constexpr bool operator()(idx3 const &lhs, idx3 const &rhs) const noexcept
            {
                return lhs[0] == rhs[0] && lhs[1] == rhs[1] && lhs[2] == rhs[2];
            }
        };
        std::unordered_map
        <
            idx3,
            idx_t,
            idx3_hash,
            idx3_equal
        > faceMap;  // lower3 -> globalFaceID
        std::vector<std::vector<idx_t>> wholeFaceMap; // globalFaceID -> face_t

        using PT = PointTraits<typename Mesh::point_t>;
        struct point_hash
        {
            std::size_t operator()(typename Mesh::point_t const &p) const noexcept
            {
                return PT::hash(p);
            }
        };
        struct point_equal
        {
            bool operator()(typename Mesh::point_t const &p1, typename Mesh::point_t const &p2) const noexcept
            {
                return PT::equal(p1, p2);
            }
        };
        std::unordered_map
        <
            typename Mesh::point_t,
            idx_t,
            point_hash,
            point_equal
        > pointMap;  // point_t -> global_pointID

        std::size_t facePointCount = 0;
        typename Mesh::idx_t const cellCount = mesh->cells();
        mCell.reserve(cellCount + 1);
        mCell.push_back(0);
        pointMap.rehash(2 * cellCount);
        faceMap.rehash(2 * cellCount);
        std::vector<idx_t> currentFace;
        for(typename Mesh::idx_t cellID = 0; cellID < cellCount; ++cellID)
        {
            typename Mesh::idx_t const faceCount = mesh->faces(cellID);
            for(typename Mesh::idx_t faceID = 0; faceID < faceCount; ++faceID)
            {
                typename Mesh::idx_t const pointCount = mesh->points(cellID, faceID);
                currentFace.clear();
                for(typename Mesh::idx_t pointID = 0; pointID < pointCount; ++pointID)
                {
                    typename Mesh::point_t const p = mesh->point(cellID, faceID, pointID);
                    auto const iter = pointMap.find(p);
                    if(iter == pointMap.end())
                    {
                        idx_t const global_pointID = static_cast<idx_t>(pointMap.size());
                        pointMap.emplace(p, global_pointID);
                        currentFace.push_back(global_pointID);
                    }
                    else
                        currentFace.push_back(iter->second);
                }
                idx_t const idxMax = static_cast<idx_t>(-1);
                idx3 lower3 = {idxMax, idxMax, idxMax};
                for(idx_t const pID : currentFace)  // yay, bubbles!
                    if(lower3[2] > pID)
                    {
                        lower3[2] = pID;
                        if(lower3[1] > lower3[2])
                        {
                            std::swap(lower3[1], lower3[2]);
                            if(lower3[0] > lower3[1])
                                std::swap(lower3[0], lower3[1]);
                        }
                    }
                auto const iter = faceMap.find(lower3);
                if(iter == faceMap.end())
                {
                    idx_t const globalFaceID = static_cast<idx_t>(faceMap.size());
                    faceMap.emplace(lower3, globalFaceID);
                    facePointCount += currentFace.size();
                    wholeFaceMap.push_back(std::move(currentFace));
                    mCellFace.push_back(globalFaceID);
                }
                else
                {
                    //assert(is_same(wholeFaceMap[iter->second], currentFace);
                    mCellFace.push_back(iter->second);
                }
            }
            mCell.push_back(static_cast<idx_t>(mCellFace.size()));
        }
        mPoint.resize(pointMap.size());
        for(auto const &[p, pointID] : pointMap)
            mPoint[pointID] = PT::template cast<point_t>(p);

        mFacePoint.reserve(facePointCount);
        mFace.reserve(wholeFaceMap.size() + 1);
        mFace.push_back(0);
        for(std::vector<idx_t> const &f : wholeFaceMap)
        {
            mFacePoint.insert(mFacePoint.end(), f.begin(), f.end());
            mFace.push_back(static_cast<idx_t>(mFacePoint.size()));
        }
    }
}
template<typename vec3, typename uint>
typename MinimalMesh<vec3, uint>::idx_t MinimalMesh<vec3, uint>::points() const noexcept
{
    return static_cast<idx_t>(mPoint.size());
}
template<typename vec3, typename uint>
typename MinimalMesh<vec3, uint>::idx_t MinimalMesh<vec3, uint>::faces() const noexcept
{
    return static_cast<idx_t>(mFace.size() - 1);
}
template<typename vec3, typename uint>
typename MinimalMesh<vec3, uint>::idx_t MinimalMesh<vec3, uint>::cells() const noexcept
{
    return static_cast<idx_t>(mCell.size() - 1);
}
template<typename vec3, typename uint>
typename MinimalMesh<vec3, uint>::point_t MinimalMesh<vec3, uint>::point(idx_t const pointID) const noexcept
{
    assert(pointID < points());
    return mPoint[pointID];
}
template<typename vec3, typename uint>
typename MinimalMesh<vec3, uint>::face_t MinimalMesh<vec3, uint>::face(idx_t const faceID) const noexcept
{
    assert(faceID < faces());
    return face_t{mFacePoint.data() + mFace[faceID], static_cast<idx_t>(mFace[faceID + 1] - mFace[faceID])};
}
template<typename vec3, typename uint>
typename MinimalMesh<vec3, uint>::cell_t MinimalMesh<vec3, uint>::cell(idx_t const cellID) const noexcept
{
    assert(cellID < cells());
    return cell_t{mCellFace.data() + mCell[cellID], static_cast<idx_t>(mCell[cellID + 1] - mCell[cellID])};
}
template<typename vec3, typename uint>
template<typename Writable>
void MinimalMesh<vec3, uint>::mm_serialize(Writable &&write) const
{
    static_assert(ImplementsWritable<Writable>::value);

    idx_t sz = static_cast<idx_t>(mPoint.size());
    write(reinterpret_cast<std::byte const *>(&sz), sizeof(idx_t));
    write(reinterpret_cast<std::byte const *>(mPoint.data()), sizeof(point_t) * sz);

    sz = static_cast<idx_t>(mFacePoint.size());
    write(reinterpret_cast<std::byte const *>(&sz), sizeof(idx_t));
    write(reinterpret_cast<std::byte const *>(mFacePoint.data()), sizeof(idx_t) * sz);

    sz = static_cast<idx_t>(mFace.size());
    write(reinterpret_cast<std::byte const *>(&sz), sizeof(idx_t));
    write(reinterpret_cast<std::byte const *>(mFace.data()), sizeof(idx_t) * sz);

    sz = static_cast<idx_t>(mCellFace.size());
    write(reinterpret_cast<std::byte const *>(&sz), sizeof(idx_t));
    write(reinterpret_cast<std::byte const *>(mCellFace.data()), sizeof(idx_t) * sz);

    sz = static_cast<idx_t>(mCell.size());
    write(reinterpret_cast<std::byte const *>(&sz), sizeof(idx_t));
    write(reinterpret_cast<std::byte const *>(mCell.data()), sizeof(idx_t) * sz);
}
template<typename vec3, typename uint>
template<typename Readable>
void MinimalMesh<vec3, uint>::mm_deserialize(Readable &&read)
{
    static_assert(ImplementsReadable<Readable>::value);
    idx_t sz;

    read(reinterpret_cast<std::byte *>(&sz), sizeof(idx_t));
    mPoint.resize(sz);
    read(reinterpret_cast<std::byte *>(mPoint.data()), sizeof(point_t) * sz);

    read(reinterpret_cast<std::byte *>(&sz), sizeof(idx_t));
    mFacePoint.resize(sz);
    read(reinterpret_cast<std::byte *>(mFacePoint.data()), sizeof(idx_t) * sz);

    read(reinterpret_cast<std::byte *>(&sz), sizeof(idx_t));
    mFace.resize(sz);
    read(reinterpret_cast<std::byte *>(mFace.data()), sizeof(idx_t) * sz);

    read(reinterpret_cast<std::byte *>(&sz), sizeof(idx_t));
    mCellFace.resize(sz);
    read(reinterpret_cast<std::byte *>(mCellFace.data()), sizeof(idx_t) * sz);

    read(reinterpret_cast<std::byte *>(&sz), sizeof(idx_t));
    mCell.resize(sz);
    read(reinterpret_cast<std::byte *>(mCell.data()), sizeof(idx_t) * sz);
}

} // namespace mesh
