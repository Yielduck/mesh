#pragma once
#include <cassert>
#include <cstddef> // std::size_t
namespace mesh
{

// if -std=c++20, use std::span<T> instead of this
template<typename T>
struct span
{
    using value     = T;
    using size_type = std::size_t;

    span() noexcept
        : m_data(nullptr)
        , m_size(0)
    {}
    span(T * const ptr, size_type const sz) noexcept
        : m_data(ptr)
        , m_size(sz)
    {}
    template<typename ContiguousContainer>
    span(ContiguousContainer &container) noexcept
        : m_data(container.data())
        , m_size(container.size())
    {}

    T *         data        ()                  const noexcept {return m_data;}
    size_type   size        ()                  const noexcept {return m_size;}

    T &         front       ()                  const noexcept {assert(m_size != 0); return m_data[0];}
    T &         back        ()                  const noexcept {assert(m_size != 0); return m_data[m_size - 1];}
    T &         operator[]  (size_type const i) const noexcept {assert(i < m_size);  return m_data[i];}

    using iterator = T *;
    iterator begin() const noexcept {return m_data;}
    iterator end  () const noexcept {return m_data + m_size;}

    span<T> subspan(size_type const offset) const noexcept
    {
        assert(offset <= m_size);
        return {m_data + offset, m_size - offset};
    }
    span<T> subspan(size_type const offset, size_type const count) const noexcept
    {
        assert(offset + count <= m_size);
        return {m_data + offset, count};
    }

private:
    T *         m_data;
    size_type   m_size;
};

} // namespace mesh
