#pragma once
#include <cstddef>  // std::size_t
namespace mesh
{

namespace detail
{

template<std::size_t sz>
struct float_t_helper
{};
template<>
struct float_t_helper<3 * sizeof(float )>
{
    using type = float;
};
template<>
struct float_t_helper<3 * sizeof(double)>
{
    using type = double;
};
template<typename point_t>
using float_t = typename float_t_helper<sizeof(point_t)>::type;

template<typename point_t>
auto area_t_helper(  signed int) -> decltype(cross(std::declval<point_t>(), std::declval<point_t>()));
template<typename point_t>
auto area_t_helper(unsigned int) -> point_t;
template<typename point_t>
using area_t = decltype(area_t_helper<point_t>(0));

template<typename point_t>
auto volume_t_helper(  signed int) -> decltype(dot(std::declval<point_t>(), std::declval<area_t<point_t>>()));
template<typename point_t>
auto volume_t_helper(unsigned int) -> float_t<point_t>;
template<typename point_t>
using volume_t = decltype(volume_t_helper<point_t>(0));

} // namespace detail

template<typename point_t>
struct PointTraits
{
    static_assert(  sizeof(point_t) == 3 * sizeof(float)
                 || sizeof(point_t) == 3 * sizeof(double)
                 );
    using float_t   = detail::float_t   <point_t>;
    using area_t    = detail::area_t    <point_t>;
    using volume_t  = detail::volume_t  <point_t>;

    static point_t construct(float_t, float_t, float_t) noexcept;
    template<typename another_point_t>
    static another_point_t cast(point_t) noexcept;

    static point_t  add     (point_t, point_t)  noexcept;
    static point_t  sub     (point_t, point_t)  noexcept;
    static point_t  div     (point_t, size_t)   noexcept;
    static area_t   cross   (point_t, point_t)  noexcept;
    static volume_t dot     (point_t, area_t)   noexcept;

    static bool     equal   (point_t, point_t)  noexcept;
    static size_t   hash    (point_t)           noexcept;

    // component-wise
    static point_t  min     (point_t, point_t)  noexcept;
    static point_t  max     (point_t, point_t)  noexcept;
};

} // namespace mesh
