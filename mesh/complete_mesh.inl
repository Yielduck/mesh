#pragma once
#include "complete_mesh.h"
#include "minimal_mesh.inl"
#include "mesh_traits.inl"
namespace mesh
{

template<typename vec3, typename uint>
void CompleteMesh<vec3, uint>::update() noexcept
{
    std::vector<std::vector<idx_t>> pointFacesID(points());
    idx_t pointFacesCount = 0;
    for(idx_t faceID = 0; faceID < faces(); ++faceID)
        for(idx_t const pointID : face(faceID))
        {
            pointFacesID[pointID].push_back(faceID);
            ++pointFacesCount;
        }

    mPointFacesID.clear();
    mPointFacesID.reserve(pointFacesCount);
    mPointFaces.clear();
    mPointFaces.reserve(points() + 1);
    mPointFaces.push_back(0);
    for(std::vector<idx_t> const &f : pointFacesID)
    {
        mPointFacesID.insert(mPointFacesID.end(), f.begin(), f.end());
        mPointFaces.push_back(static_cast<idx_t>(mPointFacesID.size()));
    }

    mFaceCells.resize(faces(), link_t{0, 1});
    for(idx_t cellID = 0; cellID < cells(); ++cellID)
        for(idx_t const faceID : cell(cellID))
        {
            auto &[cell0, cell1] = mFaceCells[faceID];
            if(cell0 != cell1)  // see first time
                cell0 = cellID;
            cell1 = cellID;
        }

    mFaceNorm.clear();
    mFaceNorm.reserve(faces());
    mFaceCenter.clear();
    mFaceCenter.reserve(faces());
    for(idx_t faceID = 0; faceID < faces(); ++faceID)
    {
        mFaceNorm.push_back(ParentTraits::faceNorm(this, faceID));
        mFaceCenter.push_back(ParentTraits::faceCenter(this, faceID));
    }
    mCellVolume.clear();
    mCellVolume.reserve(cells());
    mCellCenter.clear();
    mCellCenter.reserve(cells());
    for(idx_t cellID = 0; cellID < cells(); ++cellID)
    {
        mCellCenter.push_back(ParentTraits::cellCenter(this, cellID));
        mCellVolume.push_back(ParentTraits::cellVolume(this, cellID));
    }
}
template<typename vec3, typename uint>
void CompleteMesh<vec3, uint>::clear() noexcept
{
    mPointFacesID = {};
    mPointFaces = {};
    mFaceCells = {};
    mFaceNorm = {};
    mFaceCenter = {};
    mCellCenter = {};
    mCellVolume = {};
}
template<typename vec3, typename uint>
span<typename CompleteMesh<vec3, uint>::idx_t const> CompleteMesh<vec3, uint>::pointFaces(idx_t const pointID) const noexcept
{
    assert(pointID < points());
    return
    {
        mPointFacesID.data() + mPointFaces[pointID],
        static_cast<idx_t>(mPointFaces[pointID + 1] - mPointFaces[pointID])
    };
}
template<typename vec3, typename uint>
typename CompleteMesh<vec3, uint>::link_t CompleteMesh<vec3, uint>::faceCells(idx_t const faceID) const noexcept
{
    assert(faceID < faces());
    return mFaceCells[faceID];
}
template<typename vec3, typename uint>
typename CompleteMesh<vec3, uint>::area_t CompleteMesh<vec3, uint>::faceNorm(idx_t const faceID) const noexcept
{
    assert(faceID < faces());
    return mFaceNorm[faceID];
}
template<typename vec3, typename uint>
typename CompleteMesh<vec3, uint>::point_t CompleteMesh<vec3, uint>::faceCenter(idx_t const faceID) const noexcept
{
    assert(faceID < faces());
    return mFaceCenter[faceID];
}
template<typename vec3, typename uint>
typename CompleteMesh<vec3, uint>::point_t CompleteMesh<vec3, uint>::cellCenter(idx_t const cellID) const noexcept
{
    assert(cellID < cells());
    return mCellCenter[cellID];
}
template<typename vec3, typename uint>
typename CompleteMesh<vec3, uint>::volume_t CompleteMesh<vec3, uint>::cellVolume(idx_t const cellID) const noexcept
{
    assert(cellID < cells());
    return mCellVolume[cellID];
}

} // namespace mesh
