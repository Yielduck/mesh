#pragma once
#include <cstddef>
#include <type_traits>
namespace mesh
{

template<typename, typename = std::void_t<>>
struct ImplementsWritable
    : std::false_type
{};
template<typename T>
struct ImplementsWritable<T, std::void_t<decltype(std::declval<T>()(std::declval<std::byte const *>(), std::declval<std::size_t>()))>>
    : std::true_type
{};

} // namespace mesh
