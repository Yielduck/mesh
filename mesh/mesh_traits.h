#pragma once
#include "general_mesh_interface.h"
#include "point_traits.h"
#include <utility>
namespace mesh
{

template<typename Mesh>
struct MeshTraits
{
    static_assert(ImplementsGeneralMeshInterface<Mesh>::value);

    using point_t   = typename Mesh::point_t;
    using idx_t     = typename Mesh::idx_t;

    using face_t    = typename Mesh::face_t;
    using cell_t    = typename Mesh::cell_t;

    using area_t    = typename PointTraits<point_t>::area_t;
    using volume_t  = typename PointTraits<point_t>::volume_t;

    using edge_t    = std::pair<idx_t, idx_t>; // [pointID0, pointID1], pointID0 < pointID1
    using link_t    = std::pair<idx_t, idx_t>; // [ownerCellID, neighbourCellID]
    // in case ownerCellID == neighbourCellID, the link is boundary
    // otherwise, ownerCellID < neighbourCellID

    // methods, which do not have MeshTraits default implementation
    // are marked with `no default'

    // methods, which default implementation utilizes other methods
    // are marked with `uses X,...'

    // default implementations of methods returning spans use `static thread_local' storage (!)

    static span<idx_t const>   pointFaces(Mesh const *, idx_t pointID) noexcept; // no default
    static span<idx_t const>   pointCells(Mesh const *, idx_t pointID) noexcept; // uses pointFaces, faceCells

    static span<edge_t const>  faceEdges (Mesh const *, idx_t faceID)  noexcept;
    static link_t              faceCells (Mesh const *, idx_t faceID)  noexcept; // no default
    // let p be array of points, of which face consists;
    // then, faceNorm is the sum of
    // 0.5 * cross(p[i] - p[0], p[i - 1] - p[0]), for i in [ 2, length(p) )
    static area_t              faceNorm  (Mesh const *, idx_t faceID)  noexcept;
    static point_t             faceCenter(Mesh const *, idx_t faceID)  noexcept;

    static span<idx_t const>   cellPoints(Mesh const *, idx_t cellID)  noexcept;
    static span<edge_t const>  cellEdges (Mesh const *, idx_t cellID)  noexcept; // uses faceEdges
    static point_t             cellCenter(Mesh const *, idx_t cellID)  noexcept; // uses cellPoints (or faceCenter, if it is defined)
    static volume_t            cellVolume(Mesh const *, idx_t cellID)  noexcept; // uses faceNorm, faceCenter, cellCenter
};

} // namespace mesh
