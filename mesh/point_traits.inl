#pragma once
#include "point_traits.h"
#include <functional>   // std::hash
#include <cstring>      // std::memcmp
namespace mesh
{

namespace detail
{

template<typename point_t>
auto constructImpl(float_t<point_t> const x, float_t<point_t> const y, float_t<point_t> const z,   signed int) noexcept
    -> decltype(point_t{x, y, z})
{
    return {x, y, z};
}
template<typename point_t>
auto constructImpl(float_t<point_t> const x, float_t<point_t> const y, float_t<point_t> const z, unsigned int) noexcept
    -> point_t
{
    point_t p;
    float_t<point_t> * const f = reinterpret_cast<float_t<point_t> *>(&p);
    f[0] = x; f[1] = y; f[2] = z;
    return p;
}
template<typename point_t>
point_t construct(float_t<point_t> const x, float_t<point_t> const y, float_t<point_t> const z) noexcept
{
    return constructImpl<point_t>(x, y, z, 0);
}
template<typename point_t>
auto xImpl(point_t const p,   signed short) noexcept -> decltype(p[0])
{
    return p[0];
}
template<typename point_t>
auto xImpl(point_t const p, unsigned short) noexcept -> decltype(point_t::x)
{
    return p.x;
}
template<typename point_t>
auto xImpl(point_t const p,     signed int) noexcept -> float_t<point_t>
{
    return reinterpret_cast<float_t<point_t> const (&)[3]>(p)[0];
}
template<typename point_t>
auto yImpl(point_t const p,   signed short) noexcept -> decltype(p[1])
{
    return p[1];
}
template<typename point_t>
auto yImpl(point_t const p, unsigned short) noexcept -> decltype(point_t::y)
{
    return p.y;
}
template<typename point_t>
auto yImpl(point_t const p,     signed int) noexcept -> float_t<point_t>
{
    return reinterpret_cast<float_t<point_t> const (&)[3]>(p)[1];
}
template<typename point_t>
auto zImpl(point_t const p,   signed short) noexcept -> decltype(p[2])
{
    return p[2];
}
template<typename point_t>
auto zImpl(point_t const p, unsigned short) noexcept -> decltype(point_t::z)
{
    return p.z;
}
template<typename point_t>
auto zImpl(point_t const p,     signed int) noexcept -> float_t<point_t>
{
    return reinterpret_cast<float_t<point_t> const (&)[3]>(p)[2];
}
template<typename point_t>
float_t<point_t> x(point_t const p) noexcept {return xImpl(p, 0);}
template<typename point_t>
float_t<point_t> y(point_t const p) noexcept {return yImpl(p, 0);}
template<typename point_t>
float_t<point_t> z(point_t const p) noexcept {return zImpl(p, 0);}
template<typename point_t>
auto addImpl(point_t const p1, point_t const p2,   signed int) noexcept -> decltype(p1 + p2)
{
    return p1 + p2;
}
template<typename point_t>
auto addImpl(point_t const p1, point_t const p2, unsigned int) noexcept -> point_t
{
    return construct<point_t>(x(p1) + x(p2), y(p1) + y(p2), z(p1) + z(p2));
}
template<typename point_t>
auto subImpl(point_t const p1, point_t const p2,   signed int) noexcept -> decltype(p1 - p2)
{
    return p1 - p2;
}
template<typename point_t>
auto subImpl(point_t const p1, point_t const p2, unsigned int) noexcept -> point_t
{
    return construct<point_t>(x(p1) - x(p2), y(p1) - y(p2), z(p1) - z(p2));
}
template<typename point_t>
auto divImpl(point_t const p, size_t const n,   signed int) noexcept -> decltype(p / n)
{
    return p / n;
}
template<typename point_t>
auto divImpl(point_t const p, size_t const n, unsigned int) noexcept -> point_t
{
    return construct<point_t>
    (
        x(p) / static_cast<float_t<point_t>>(n),
        y(p) / static_cast<float_t<point_t>>(n),
        z(p) / static_cast<float_t<point_t>>(n)
    );
}
template<typename point_t>
auto crossImpl(point_t const p1, point_t const p2,   signed int) noexcept -> decltype(cross(p1, p2))
{
    return cross(p1, p2);
}
template<typename point_t>
auto crossImpl(point_t const p1, point_t const p2, unsigned int) noexcept -> area_t<point_t>
{
    return construct<point_t>
    (
        y(p1) * z(p2) - z(p1) * y(p2),
        z(p1) * x(p2) - x(p1) * z(p2),
        x(p1) * y(p2) - y(p1) * x(p2)
    );
}
template<typename point_t>
auto dotImpl(point_t const p, area_t<point_t> const a,   signed int) noexcept -> decltype(dot(p, a))
{
    return dot(p, a);
}
template<typename point_t>
auto dotImpl(point_t const p, area_t<point_t> const a, unsigned int) noexcept -> volume_t<point_t>
{
    return x(p) * x(a) + y(p) * y(a) + z(p) * z(a);
}

} // namespace detail

template<typename point_t>
point_t PointTraits<point_t>::construct(float_t const x, float_t const y, float_t const z) noexcept
{
    return detail::construct<point_t>(x, y, z);
}
template<typename point_t>
template<typename another_point_t>
another_point_t PointTraits<point_t>::cast(point_t const p) noexcept
{
    return detail::construct<another_point_t>
    (
        static_cast<detail::float_t<another_point_t>>(detail::x<point_t>(p)),
        static_cast<detail::float_t<another_point_t>>(detail::y<point_t>(p)),
        static_cast<detail::float_t<another_point_t>>(detail::z<point_t>(p))
    );
}
template<typename point_t>
point_t PointTraits<point_t>::add(point_t const p1, point_t const p2) noexcept
{
    return detail::addImpl(p1, p2, 0);
}
template<typename point_t>
point_t PointTraits<point_t>::sub(point_t const p1, point_t const p2) noexcept
{
    return detail::subImpl(p1, p2, 0);
}
template<typename point_t>
point_t PointTraits<point_t>::div(point_t const p, size_t const n) noexcept
{
    return detail::divImpl(p, n, 0);
}
template<typename point_t>
detail::area_t<point_t> PointTraits<point_t>::cross(point_t const p1, point_t const p2) noexcept
{
    return detail::crossImpl(p1, p2, 0);
}
template<typename point_t>
detail::volume_t<point_t> PointTraits<point_t>::dot(point_t const p, area_t const a) noexcept
{
    return detail::dotImpl(p, a, 0);
}
template<typename point_t>
bool PointTraits<point_t>::equal(point_t const p1, point_t const p2) noexcept
{
    return std::memcmp(&p1, &p2, sizeof(point_t)) == 0;
}
template<typename point_t>
size_t PointTraits<point_t>::hash(point_t const p) noexcept
{
    return std::hash<detail::float_t<point_t>>{}(detail::x(p))
         ^ std::hash<detail::float_t<point_t>>{}(detail::y(p))
         ^ std::hash<detail::float_t<point_t>>{}(detail::z(p));
}
template<typename point_t>
point_t PointTraits<point_t>::min(point_t const p1, point_t const p2) noexcept
{
    return detail::construct<point_t>
    (
        std::min(detail::x<point_t>(p1), detail::x<point_t>(p2)),
        std::min(detail::y<point_t>(p1), detail::y<point_t>(p2)),
        std::min(detail::z<point_t>(p1), detail::z<point_t>(p2))
    );
}
template<typename point_t>
point_t PointTraits<point_t>::max(point_t const p1, point_t const p2) noexcept
{
    return detail::construct<point_t>
    (
        std::max(detail::x<point_t>(p1), detail::x<point_t>(p2)),
        std::max(detail::y<point_t>(p1), detail::y<point_t>(p2)),
        std::max(detail::z<point_t>(p1), detail::z<point_t>(p2))
    );
}

} // namespace mesh
