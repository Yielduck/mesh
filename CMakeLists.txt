cmake_minimum_required(VERSION 3.0)
project(mesh)

set(HEADERS
    mesh/complete_mesh.h
    mesh/complete_mesh.inl
    mesh/general_mesh_interface.h
    mesh/mesh_traits.h
    mesh/mesh_traits.inl
    mesh/mesh_view.h
    mesh/minimal_mesh.h
    mesh/minimal_mesh.inl
    mesh/point_traits.h
    mesh/point_traits.inl
    mesh/readable.h
    mesh/simple_mesh_interface.h
    mesh/span.h
    mesh/writable.h
)

add_library(${PROJECT_NAME} INTERFACE)
target_include_directories(${PROJECT_NAME} INTERFACE .)
target_compile_features(${PROJECT_NAME} INTERFACE cxx_std_17)
